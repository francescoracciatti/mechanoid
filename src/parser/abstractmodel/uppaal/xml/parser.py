"""
This file provides a parser for the xml description of UPPAAL models (http://www.uppaal.org/).
"""

import logging
import os
import re
from typing import List, Dict, Union
from xml.etree import ElementTree

logger = logging.getLogger(__name__)


class Channel(object):
    """
    This class models the broadcast channel of the UPPAAL model.
    """

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, message_identifier: str, message_type: str) -> None:
        """
        Ctor.

        :param message_identifier: the identifier
        :param message_type: the type
        """
        self.identifier = message_identifier
        self.type = message_type


class GlobalDeclaration(object):
    """
    This class models the global declaration tag of the UPPAAL model.
    """

    # Pattern for matching empty lines
    p_empty = re.compile(r'^\s*$')

    # Pattern for matching comments
    p_comment = re.compile(r'^\s*//')

    # Pattern for matching clock
    # Capturing group:
    #  group(1) : the identifier of the clock
    p_clock = re.compile(r'^\s*clock\s*(\w+);\s*')

    # Pattern for matching broadcast chan
    # Capturing group:
    #  group(1) : the identifier
    #  group(2) : the type
    p_broadcast = re.compile(r'^\s*broadcast\s+chan\s+(\w+)\s*\[?(\w+)\]?\s*;\s*')

    # Pattern for matching aliases
    # Example:
    #   typedef int [0, N] alias;
    # Capturing group:
    #   group(1) : 'typedef'
    #   group(2) : the type
    #   group(3) : the alias
    p_typedef = re.compile(r'^\s*(typedef)\s*(\w*)\s*\[\s*\d+\s*,\s*\w*\s*\]\s*(\w*);\s*$')

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, text: str) -> None:
        """
        Ctor.

        :param text: the raw text
        """
        self.text = text
        self.aliases: List[str] = []
        self.globals: List[str] = []
        # noinspection PyTypeChecker
        self.channel: Channel = None
        self.clock: str = ''
        self.alias_to_type: Dict[str, str] = {}

        # Gets aliases and globals
        lines = text.splitlines(keepends=True)
        for line in lines:
            # Skips comments and empty lines
            if self.p_empty.match(line) or self.p_comment.match(line):
                continue

            # Gets the clock
            match_clock = self.p_clock.match(line)
            if match_clock:
                self.clock = match_clock.group(1)
                continue

            # Gets the channel
            match_broadcast = self.p_broadcast.match(line)
            if match_broadcast:
                self.channel = Channel(match_broadcast.group(1), match_broadcast.group(2))
                continue

            # Gets aliases
            match_typedef = self.p_typedef.match(line)
            if match_typedef:
                alias = f'{match_typedef.group(1)} {match_typedef.group(2)} {match_typedef.group(3)};\n'
                self.aliases.append(alias)
                self.alias_to_type[match_typedef.group(3)] = match_typedef.group(2)
                continue

            # Gets globals as raw
            self.globals.append(line.lstrip())

        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")


class Coord(object):
    """
    This class provides the position of a node in a Cartesian coordinate system.
    """

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, x: float, y: float, z: float = 0.0) -> None:
        """
        Ctor.

        :param x: the x coordinate
        :param y: the y coordinate
        :param z: the z coordinate, 0.0 by default
        """
        self.x = x
        self.y = y
        self.z = z
        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")


class Parameter(object):
    """
    This class models a parameter.
    """

    # Pattern for matching the type and the identifier of the parameter
    p_parameter = re.compile(r'^\s*(\w+)\s*(\w+)\s*')

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, text: str) -> None:
        """
        Ctor.

        :param text: the raw text
        """
        self.text = text

        match_parameter = self.p_parameter.match(text)
        if match_parameter:
            self.type = match_parameter.group(1)
            self.identifier = match_parameter.group(2)
        else:
            error_msg = f"Cannot recognize type and identifier from the parameter: {text}"
            logger.error(error_msg)
            raise ValueError(error_msg)

        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")


class Declaration(object):
    """
    This class models a declaration.
    """

    # Pattern for matching empty lines
    p_empty = re.compile(r'^\s*$')

    # Pattern for matching comments
    p_comment = re.compile(r'^\s*//')

    # Pattern for matching the annotation '@Attributes'
    p_annotation_attributes = re.compile(r'^\s*//\s*@Attributes\s*')

    # Pattern for matching the annotation '@Functions'
    p_annotation_functions = re.compile(r'^\s*//\s*@Functions\s*')

    # Pattern for matching the placeholder '@Trace'
    # Capturing groups
    #  group(1) : the string of the trace message
    #  group(2) : the variable to be traced
    p_trace = re.compile(r'^\s*//\s*@Trace.*')

    # Pattern for matching local attributes
    p_attribute = re.compile(r'^\s*(.*);\s*')

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, text: str) -> None:
        """
        Ctor.

        From raw text, it builds a list of local attributes and functions.

        Note that the raw test shall be annotated with the annotations:
         - @Attributes, that marks the beginning of the section containing the attributes
         - @Functions, that marks the beginning of the section containing the local functions
        Remark that the attributes (@Attributes) shall be declared before the functions (@Functions).

        Attributes and functions are stored as raw text.

        :param text: the raw declaration
        """
        self.text = text
        # The attributes
        self.attributes: List[str] = []
        # The functions
        self.functions: List[str] = []
        # The functions' signatures (signatures[i] is the signature of functions[i])
        self.signatures: List[str] = []

        lines = text.splitlines(keepends=True)

        # Gets local attributes
        store_attributes = False
        self.attributes: List[str] = []
        for line in lines:
            # Matches the annotation '@Attributes'
            if self.p_annotation_attributes.match(line):
                store_attributes = True

            # The attributes section ends
            if self.p_annotation_functions.match(line):
                break

            # Skips empty lines
            if self.p_empty.match(line):
                continue

            # Skips comments
            if self.p_comment.match(line):
                continue

            # Matches attributes
            if store_attributes:
                match_attribute = self.p_attribute.match(line)
                if match_attribute:
                    self.attributes.append(match_attribute.group(1))

        # Gets local functions
        function = ''
        function_opened = False
        function_closed = False
        curvy_counter = 0

        store_functions = False
        self.functions: List[str] = []
        for line in lines:
            # Matches the annotation '@Functions'
            if self.p_annotation_functions.match(line):
                store_functions = True

            # Skips empty lines
            if self.p_empty.match(line):
                continue

            # Skips comments but @Trace annotations
            if self.p_comment.match(line) and not self.p_trace.match(line):
                continue

            # Matches functions
            if store_functions:
                curvy_opened = line.count('{')
                curvy_closed = line.count('}')
                curvy_counter += curvy_opened - curvy_closed

                function += line

                # Recognizes function opening
                if not function_opened and curvy_opened > 0:
                    function_opened = True

                # Recognizes function closing
                if function_opened and curvy_counter == 0:
                    function_closed = True

                # Stores current function and prepares for the next one
                if function_closed:
                    self.functions.append(function)
                    function = ''
                    function_opened = False
                    function_closed = False
                    curvy_counter = 0

        # Pattern for matching the functions' signatures
        p_signature = re.compile(r'^(.*)\s*{\s*.*')

        # Gets the signature of the functions
        for function in self.functions:
            signature = function.splitlines()[0]
            if '{' in signature:
                match_signature = p_signature.match(signature)
                if match_signature:
                    signature = match_signature.group(1)
                else:
                    error_msg = f"Cannot get signature from function: {function}"
                    logger.error(error_msg)
                    raise RuntimeError(error_msg)
            signature = signature.strip()
            self.signatures.append(signature)

        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")


class Transition(object):
    """
    This class models a Transition.
    """

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self,
                 source: str,
                 target: str,
                 guard: Union[str, None],
                 synchronization: Union[str, None],
                 assignments: Union[str, None]) -> None:
        """
        Ctor.

        :param source: the source location
        :param target: the target location
        :param guard: the guard
        :param synchronization: the synchronization
        :param assignments: the assignment
        """
        self.source = source
        self.target = target
        self.guard = guard
        self.synchronization = synchronization
        self.assignments = assignments


class Template(object):
    """
    This class models templates.
    """

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self) -> None:
        """
        Ctor.
        """
        self.name: str = ''
        # noinspection PyTypeChecker
        self.parameter: Parameter = None
        # noinspection PyTypeChecker
        self.declaration: Declaration = None
        self.locations: List[str] = []
        self.init: str = ''
        self.transitions: List[Transition] = []

        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")

    def set_name(self, text: str) -> None:
        """
        Sets the name of the template.

        :param text: the raw text of the name
        """
        self.name = text

    def set_parameter(self, text: str) -> None:
        """
        Sets the parameter of the template.

        :param text: the raw text of the parameter
        """
        self.parameter = Parameter(text)

    def set_declaration(self, text: str) -> None:
        """
        Sets the declaration of the template.

        :param text: the raw text of the declaration
        """
        self.declaration = Declaration(text)

    def add_location(self, text: str) -> None:
        """
        Add a location to the template.

        :param text: the raw text of the location
        """
        self.locations.append(text)

    def set_init(self, text: str) -> None:
        """
        Sets the init of the template.

        :param text: the raw text of the init
        """
        self.init = text

    def add_transition(self, transition: Transition) -> None:
        """
        Adds a transition to the template.

        :param transition: the transition
        """
        self.transitions.append(transition)


class Node(object):
    """
    This class models system nodes.
    """

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, coord: Coord, identifier: str, template_name: str, argument: str) -> None:
        """
        Ctor.

        :param coord: the coordinates of the node
        :param identifier: the identifier of the node
        :param template_name: the name of the template
        :param argument: the identifier of the function

        """
        self.coord = coord
        self.identifier = identifier
        self.template_name = template_name
        self.argument = argument
        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")


class System(object):
    """
    This class describes the system model.
    """

    # Pattern for matching empty lines
    p_empty = re.compile(r'^\s*$')

    # Pattern for matching the @Coord annotation
    # Example:
    #  // @Coord(50, 10, 0)
    #
    # Capturing groups:
    #  group(1) : x coord
    #  group(2) : y coord
    #  group(3) : z coord
    p_coord = re.compile(r'^\s*//\s*@Coord\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)')

    # Pattern for matching the node
    # Example:
    #  node1 := myfunction(1);
    #
    # Capturing groups:
    #  group(1) : node identifier
    #  group(2) : template
    #  group(3) : node argument
    p_node = re.compile(r'^\s*(\w*)\s*:=\s*(\w*)\((\d*)\)\s*;')

    # Pattern for matching the system definition
    # Example:
    #  system source, node1, node2;
    #
    # Capturing group:
    #  group(1) : process identifiers separated each other by means of commas and spaces
    p_system = re.compile(r'^\s*system\s*(.*);')

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self, text: str) -> None:
        """
        Ctor.

        :param text: the raw text
        """
        self.text = text
        self.nodes: List[Node] = []

        # Gets the list of nodes
        identifier_to_node: Dict[str, Node] = {}
        system_node_identifiers: List[str] = []
        # noinspection PyTypeChecker
        current_coord: Coord = None
        lines = text.splitlines(keepends=True)
        for line in lines:
            # Skips empty lines
            if self.p_empty.match(line):
                continue

            # Gets the system
            match_system = self.p_system.match(line)
            if match_system:
                system_node_identifiers = match_system.group(1).replace(' ', '').split(',')

            # First, look for @Coord annotation
            if not current_coord:
                match_coord = self.p_coord.match(line)
                if match_coord:
                    x = float(match_coord.group(1))
                    if x < 0.0:
                        error_msg = f"Coordinate x cannot be negative: {str(x)}"
                        logger.error(error_msg)
                        raise ValueError(error_msg)

                    y = float(match_coord.group(2))
                    if y < 0.0:
                        error_msg = f"Coordinate y cannot be negative: {str(y)}"
                        logger.error(error_msg)
                        raise ValueError(error_msg)

                    z = float(match_coord.group(3))
                    if z < 0.0:
                        error_msg = f"Coordinate z cannot be negative: {str(z)}"
                        logger.error(error_msg)
                        raise ValueError(error_msg)

                    current_coord = Coord(x, y, z)
                    continue
            else:  # Then, looks for the node
                match_node = self.p_node.match(line)
                if match_node:
                    identifier = match_node.group(1)
                    template = match_node.group(2)
                    argument = match_node.group(3)
                    node = Node(current_coord, identifier, template, argument)
                    identifier_to_node[identifier] = node
                    # noinspection PyTypeChecker
                    current_coord = None
                else:
                    error_msg = "Wrong syntax, missing node after @Coord annotation"
                    logger.error(error_msg)
                    raise ValueError(error_msg)

        logger.debug(f"defined nodes: {str(identifier_to_node)}")
        logger.debug(f"system nodes: {str(system_node_identifiers)}")

        self.nodes = [identifier_to_node[identifier]
                      for identifier in system_node_identifiers if identifier in identifier_to_node.keys()]

        logger.debug(f"{self.__class__.__name__} instance: {self.__repr__()}")


class XmlUppaalParser(object):
    """
    This class parses the xml description of UPPAAL models.
    """

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def __init__(self) -> None:
        """
        Ctor.
        """
        # The xml source
        self.xml_source: str = ''
        # noinspection PyTypeChecker
        self.global_declaration: GlobalDeclaration = None
        # noinspection PyTypeChecker
        self.system: System = None
        self.name_to_template: Dict[str: Template] = {}

    def _get_global_declaration(self, tree: ElementTree) -> None:
        """
        Gets the global description.
        """
        declaration = tree.find('declaration')
        self.global_declaration = declaration.text

    def _parse_global_description(self, tree: ElementTree) -> None:
        """
        Parses the global description tag.

        :param tree: the element tree of the xml
        """
        self.global_declaration = GlobalDeclaration(tree.find('declaration').text)

    def _parse_system(self, tree: ElementTree) -> None:
        """
        Parses the system tag.

        :param tree: the element tree of the xml
        """
        self.system = System(tree.find('system').text)

    def _parse_templates(self, tree: ElementTree) -> None:
        """
        Parses the templates.
        :param tree: the element tree of the xml
        """
        # Loops over 'template' tags
        for elem in tree.iter():
            if elem.tag == 'template':
                template = Template()

                for child in elem.getchildren():
                    # Sets the name
                    if child.tag == 'name':
                        template.set_name(child.text)
                        continue

                    # Sets the parameter
                    if child.tag == 'parameter':
                        template.set_parameter(child.text)
                        continue

                    # Adds a location
                    if child.tag == 'location':
                        template.add_location(child.attrib['id'])
                        continue

                    # Sets the init
                    if child.tag == 'init':
                        template.set_init(child.attrib['ref'])

                    # Sets the declaration
                    if child.tag == 'declaration':
                        template.set_declaration(child.text)
                        continue

                    # Adds the transition
                    if child.tag == 'transition':
                        source = child.find('source').attrib['ref']
                        target = child.find('target').attrib['ref']
                        guard = None
                        synchronization = None
                        assignment = None
                        for transition_child in child.getchildren():
                            if transition_child.tag == 'label':
                                if transition_child.attrib['kind'] == 'guard':
                                    guard = transition_child.text
                                    continue
                                if transition_child.attrib['kind'] == 'synchronisation':
                                    synchronization = transition_child.text
                                    continue
                                if transition_child.attrib['kind'] == 'assignment':
                                    assignment = transition_child.text
                        transition = Transition(source, target, guard, synchronization, assignment)
                        template.add_transition(transition)

                self.name_to_template[template.name] = template

    def parse(self, path_src_xml: str) -> None:
        """
        Parses the source XML file.

        :param path_src_xml: the path to the source XML file.
        :raises: ValueError: raised when the XML file does not exist.
        """
        if not os.path.isfile(path_src_xml):
            error_msg = f"XML file {path_src_xml} not found"
            logger.error(error_msg)
            raise ValueError(error_msg)

        tree = ElementTree.parse(path_src_xml)

        self.xml_source = path_src_xml

        # Parses the global description tag
        self._parse_global_description(tree)

        # Parses the system tag
        self._parse_system(tree)

        # Parses the template tag
        self._parse_templates(tree)
