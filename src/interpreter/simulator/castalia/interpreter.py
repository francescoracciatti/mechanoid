"""
This file provide the interpreter for generating the OMNeT++/Castalia files related to a given UPPAAL model.
"""

import logging
import os
import re
from enum import Enum
from typing import List, Dict, Union

from src.interpreter.simulator.utility.utils import get_heading, upper_first_letter, lower_first_letter, \
    is_global_identifier
from src.parser.abstractmodel.uppaal.xml.parser import XmlUppaalParser, System

logger = logging.getLogger(__name__)


class Layer(Enum):
    """
    This class enums the TCP/IP layers.
    """
    APPLICATION = 5
    TRANSPORT = 4
    INTERNET = 3
    LINK = 2


def get_transition_name(index: int, suffix: str, prefix: str = 'Transition', separator: str = '_') -> str:
    """
    Gets the name of the concrete transition.

    :param index: the index
    :param prefix: the prefix of the name, 'Transition_' by default
    :param suffix: the suffix of the name, 'Transition_' by default
    :param separator: the separator
    :return: the name of the concrete transition
    """
    return f'{prefix}{separator}{suffix}{separator}{index}'


# TODO refactor the class below
class Interpreter(object):
    """
    This class provides the interpreter for generating the OMNeT++/Castalia files related to a given UPPAAL model.
    """

    # The folder for storing application source utilities
    folder_util_application = 'utils'

    # The folder for storing simulation files
    folder_sim_application = 'uppaalNetwork'

    # The relative path for folder storing template files
    rel_path_template = 'template/'

    # The relative path for storing application source files
    rel_path_application_src = 'src/node/application/'

    # The relative path for storing application simulation files
    rel_path_application_sim = 'Simulations/'

    @staticmethod
    def _get_path_application_util(path_dst: str) -> str:
        """
        Gets the path to the util dir in application source tree.

        :param path_dst: the base path
        :return: the path to the util dir in application source tree
        """
        return os.path.join(os.path.join(path_dst, Interpreter.rel_path_application_src),
                            Interpreter.folder_util_application)

    @staticmethod
    def _get_application_folder_name(path_dst: str, template_name: str) -> str:
        """
        Gets the name of the folder for the application.

        :param path_dst: the destination path
        :param template_name: the name of the template, namely the application
        :return: the path for the template
        """
        return os.path.join(path_dst, lower_first_letter(template_name))

    @staticmethod
    def _make_application_directories(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Makes the destination dirs.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        # Makes src application tree
        logger.info(f"Making folders {Interpreter.rel_path_application_src} in {path_dst}")
        path_base_src = os.path.join(path_dst, Interpreter.rel_path_application_src)
        os.makedirs(path_base_src)

        # Makes one dir in src application tree for each template
        for template_name in parser.name_to_template.keys():
            os.mkdir(Interpreter._get_application_folder_name(path_base_src, template_name))

        # Adds utils dir to application tree
        logger.info(f"Making folder {Interpreter.folder_util_application} into {path_base_src}")
        os.makedirs(Interpreter._get_path_application_util(path_dst))

        # Makes sim application tree
        logger.info(f"Making folders {Interpreter.rel_path_application_sim} in {path_dst}")
        path_base_sim = os.path.join(
            os.path.join(path_dst, Interpreter.rel_path_application_sim),
            Interpreter.folder_sim_application)
        os.makedirs(path_base_sim)

    @staticmethod
    def _generate_global_dependencies(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates the files uppaal_globals.h and uppaal_globals.cc.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """

        class Variable(object):
            """
            This class models the type of the variable.
            """

            def __init__(self,
                         variable_type: str,
                         variable_identifier: str,
                         variable_value: Union[str, None],
                         is_vector: bool,
                         variable_size: Union[str, None],
                         is_const: bool = False) -> None:
                """
                Ctor.

                :param variable_type: the type of the variable
                :param variable_identifier: the identifier of the variable
                :param variable_value: the value of the variable
                :param is_vector: True if the variable is a vector, False otherwise
                :param variable_size: the size of the variable
                :param is_const: True if the variable is const, False by default
                """
                self.type = variable_type
                self.identifier = variable_identifier
                self.value = variable_value
                self.is_vector = is_vector
                self.size = variable_size
                self.is_const = is_const

        # Pattern for matching a scalar
        # Capturing groups:
        #  group(1) : the type
        #  group(2) : the identifier
        #  group(3) : the value
        p_const_scalar = re.compile(r'^\s*const\s*(\w+)\s*(\w+)\s*=\s*(\w+)\s*;\s*')

        # Pattern for matching a scalar
        # Capturing groups:
        #  group(1) : the type
        #  group(2) : the identifier
        #  group(3) : the value
        p_scalar = re.compile(r'^\s*(\w+)\s*(\w+)\s*=\s*(\w+)\s*;\s*')

        # Pattern for matching a vector
        # Capturing groups:
        #  group(1) : the type
        #  group(2) : the identifier
        #  group(3) : the size
        p_vector = re.compile(r'^\s*(\w+)\s*(\w+)\s*\[\s*(.*)\s*\]\s*;\s*')

        # The list of global variables
        global_variables: List[Variable] = []

        for glob in parser.global_declaration.globals:
            # Matches a scalar
            match_const_scalar = p_const_scalar.match(glob)
            if match_const_scalar:
                global_variables.append(
                    Variable(match_const_scalar.group(1),
                             match_const_scalar.group(2),
                             match_const_scalar.group(3),
                             False,
                             None,
                             True))
                continue

            # Matches a scalar
            match_scalar = p_scalar.match(glob)
            if match_scalar:
                global_variables.append(
                    Variable(match_scalar.group(1),
                             match_scalar.group(2),
                             match_scalar.group(3),
                             False,
                             None))
                continue

            # Matches a vector
            match_vector = p_vector.match(glob)
            if match_vector:
                global_variables.append(
                    Variable(match_vector.group(1),
                             match_vector.group(2),
                             None,
                             True,
                             match_vector.group(3)))

        # Writes the header file
        filename = 'uppaal_globals.h'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # Writes the heading
        heading = get_heading(parser.xml_source, "//")
        file_generated.write(heading)

        # Pattern for matching the placeholder '@Aliases'
        p_aliases = re.compile(r'^\s*@Aliases\s*$')

        # Pattern for matching the placeholder '@Globals'
        p_globals = re.compile(r'^\s*@Globals\s*$')

        lines = content_template.splitlines(keepends=True)
        for line in lines:
            # Replaces the placeholders
            if p_aliases.match(line):
                for alias in parser.global_declaration.aliases:
                    file_generated.write(alias)
                    logging.debug(f"Into {filename} stored: {alias}")

            elif p_globals.match(line):
                for var in global_variables:
                    line_var = 'extern '
                    if var.is_vector:
                        line_var += f'{var.type} {var.identifier}[];\n'
                    else:
                        if var.is_const:
                            line_var += f'const {var.type} {var.identifier};\n'
                        else:
                            line_var += f'{var.type} {var.identifier};\n'

                    file_generated.write(line_var)
                    logging.debug(f"Into {filename} stored: {line_var}")

            else:
                file_generated.write(line)
                logging.debug(f"Into {filename} stored: {line}")

        file_generated.close()
        logging.info(f"File {filename} closed")

        # Writes the implementation file
        filename = 'uppaal_globals.cc'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # Writes the heading
        heading = get_heading(parser.xml_source, '//')
        file_generated.write(heading)

        # Pattern for matching the placeholder '@Definitions'
        p_definitions = re.compile(r'^\s*@Definitions\s*$')

        lines = content_template.splitlines(keepends=True)
        for line in lines:
            # Replaces the placeholders
            if p_definitions.match(line):
                for var in global_variables:
                    line_var = ''
                    if var.is_vector:
                        line_var += f'{var.type} {var.identifier}[{var.size}];\n'
                    else:
                        if var.is_const:
                            line_var += f'const {var.type} {var.identifier} = {var.value};\n'
                        else:
                            line_var += f'{var.type} {var.identifier} = {var.value};\n'

                    file_generated.write(line_var)
                    logging.debug(f"Into {filename} stored: {line_var}")

            else:
                file_generated.write(line)
                logging.debug(f"Into {filename} stored: {line}")

        file_generated.close()
        logging.info(f"File {filename} closed")

    @staticmethod
    def _calculate_simulation_field(system: System) -> [float, float, float]:
        """
        Calculates the simulation field.

        :param system: the model of the system
        :return: the size of the simulation field on the x, y and z axis
        """
        padding_space = 10

        max_x = system.nodes[0].coord.x
        max_y = system.nodes[0].coord.y
        max_z = system.nodes[0].coord.z

        for node in system.nodes:
            if node.coord.x > max_x:
                max_x = node.coord.x

            if node.coord.y > max_y:
                max_y = node.coord.y

            if node.coord.z > max_z:
                max_z = node.coord.z

        return [max_x + padding_space,
                max_y + padding_space,
                max_z + padding_space]

    @staticmethod
    def _generate_packet_msg(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates the .msg file.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename_template = 'UppaalPacket.msg'

        file_generated = open(os.path.join(path_dst, filename_template), 'w')
        logging.info(f"File {filename_template} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename_template]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # Writes the heading
        heading = get_heading(parser.xml_source, '//')
        file_generated.write(heading)

        # Pattern for matching the placeholder '@ChannelType'
        p_channel_type = re.compile(r'^\s*@ChannelType\s*(.*)$')

        # Writes the file
        lines = content_template.splitlines(keepends=True)
        for line in lines:
            # Replaces the placeholders
            if p_channel_type.match(line):
                channel_type = parser.global_declaration.channel.type
                if parser.global_declaration.channel.type not in ('bool', 'int', 'double', 'string'):
                    channel_type = parser.global_declaration.alias_to_type[channel_type]
                line = line.replace('@ChannelType', channel_type)

            file_generated.write(line)
            logging.debug(f"Into {filename_template} stored: {line}")

        file_generated.close()
        logging.info(f"File {filename_template} closed")

    @staticmethod
    def _generate_ned_module(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates one .ned modules for each uppaal template.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename_template = 'Application.ned'

        # Generates one .ned module for each uppaal template
        for name in parser.name_to_template.keys():
            filename = f'{upper_first_letter(name)}.ned'
            path_file = os.path.join(Interpreter._get_application_folder_name(path_dst, name), filename)
            file_generated = open(path_file, 'w')
            logging.info(f"File {filename} opened")

            # Gets the file template
            pathlist = [os.path.dirname(__file__),
                        Interpreter.rel_path_template,
                        Interpreter.rel_path_application_src,
                        filename_template]
            path_file_template = os.path.join(*pathlist)
            with open(path_file_template, 'r') as file_template:
                content_template = file_template.read()

            # Writes the heading
            heading = get_heading(parser.xml_source, '//')
            file_generated.write(heading)

            # Pattern for matching the placeholder '@ApplicationPath'
            p_application_name = re.compile(r'^(.*)@ApplicationPath')

            # Pattern for matching the placeholder '@ModuleName'
            p_module_name = re.compile(r'^(.*)@ModuleName')

            # Pattern for matching the placeholder '@TemplateArgument'
            p_template_argument = re.compile(r'^(.*)@TemplateArgument')

            # Pattern for matching the placeholder '@ApplicationID'
            p_application_id = re.compile(r'^(.*)@ApplicationID')

            # Writes the file
            lines = content_template.splitlines(keepends=True)
            for line in lines:
                # Replaces the placeholder @ApplicationPath
                if p_application_name.match(line):
                    line = line.replace('@ApplicationPath', lower_first_letter(name))

                # Replaces the placeholder @ModuleName
                if p_module_name.match(line):
                    line = line.replace('@ModuleName', upper_first_letter(name))

                # Replaces the placeholder @TemplateArgument
                if p_template_argument.match(line):
                    # Gets a ned compliant type
                    if parser.name_to_template[name].parameter:
                        parameter_type = parser.name_to_template[name].parameter.type
                        if parameter_type not in ('bool', 'int', 'double', 'string'):
                            p_alias = re.compile(r'^\s*(typedef)\s*(\w*)\s*(\w*)')
                            for alias in parser.global_declaration.aliases:
                                match_alias = p_alias.match(alias)
                                if match_alias:
                                    parameter_type = match_alias.group(2)
                        parameter_identifier = parser.name_to_template[name].parameter.identifier
                        ned_parameter = f'{parameter_type} {parameter_identifier};'
                        line = line.replace('@TemplateArgument', ned_parameter)
                    else:
                        line = line.replace('@TemplateArgument', '')

                # Replaces the placeholder @ApplicationID
                if p_application_id.match(line):
                    line = line.replace('@ApplicationID', lower_first_letter(name))

                file_generated.write(line)
                logging.debug(f"Into {filename} stored: {line}")

            file_generated.close()
            logging.info(f"File {filename} closed")

    @staticmethod
    def _generate_application_headers(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates one header files for each uppaal template.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename_template = "Application.h"

        # Generates one header for each uppaal template
        for name in parser.name_to_template.keys():
            filename = f'{upper_first_letter(name)}.h'
            path_dst_file = os.path.join(Interpreter._get_application_folder_name(path_dst, name), filename)
            file_generated = open(path_dst_file, 'w')
            logging.info(f"File {filename} opened")

            # Gets the file template
            pathlist = [os.path.dirname(__file__),
                        Interpreter.rel_path_template,
                        Interpreter.rel_path_application_src,
                        filename_template]
            path_file_template = os.path.join(*pathlist)
            with open(path_file_template, 'r') as file_template:
                content_template = file_template.read()

            # Writes the heading
            heading = get_heading(parser.xml_source, '//')
            file_generated.write(heading)

            # Pattern for matching the placeholder '@OpenIncludeGuard'
            p_open_include_guard = re.compile(r'^(.*)@OpenIncludeGuard')

            # Pattern for matching the placeholder '@IncludeTransitions'
            p_include_transitions = re.compile(r'^\s*@IncludeTransitions')

            # Pattern for matching the placeholder '@FriendList'
            p_friend_list = re.compile(r'^\s*@FriendList')

            # Pattern for matching the placeholder '@ClassName'
            p_class_name = re.compile(r'^(.*)@ClassName')

            # Pattern for matching the placeholder '@Methods'
            p_methods = re.compile(r'^(.*)@Methods')

            # Pattern for matching the placeholder '@Attributes'
            p_attributes = re.compile(r'^(.*)@Attributes')

            # Pattern for matching the placeholder '@Parameters'
            p_parameters = re.compile(r'^\s*@Parameters')

            # Pattern for matching the placeholder '@CloseIncludeGuard'
            p_close_include_guard = re.compile(r'^(.*)@CloseIncludeGuard')

            # The list of friend classes
            friend_list: List[str] = []

            # Writes the file
            lines = content_template.splitlines(keepends=True)
            for line in lines:
                # Replaces the placeholder @OpenIncludeGuard
                if p_open_include_guard.match(line):
                    open_include_guard = f'#ifndef {name.upper()}_H\n'
                    open_include_guard += f'#define {name.upper()}_H\n'
                    line = open_include_guard

                # Replaces the placeholder @IncludeTransitions
                if p_include_transitions.match(line):
                    line = ''
                    i = 0
                    transition_suffix = upper_first_letter(name)
                    for _ in parser.name_to_template[name].transitions:
                        transition_class_name = get_transition_name(i, suffix=transition_suffix)
                        friend_list.append(transition_class_name)
                        line += f'#include \"{transition_class_name}.h\"\n'
                        i += 1

                # Replaces the placeholder @FriendList
                if p_friend_list.match(line):
                    line = ''
                    for friend in friend_list:
                        line += f'friend class {friend};\n'

                # Replaces the placeholder @ClassName
                if p_class_name.match(line):
                    line = line.replace('@ClassName', upper_first_letter(name))

                # Replaces the placeholder @Methods
                if p_methods.match(line):
                    line = ""
                    if not parser.name_to_template[name].declaration.signatures:
                        line = f'{4 * " "}// No template\'s functions\n'
                    for signature in parser.name_to_template[name].declaration.signatures:
                        line += f'{4 * " "}{signature};\n'

                # Replaces the placeholder @Attributes
                if p_attributes.match(line):
                    line = ''
                    if not parser.name_to_template[name].declaration.attributes:
                        line = f'{4 * " "}// No template\'s attributes\n'
                    for attribute in parser.name_to_template[name].declaration.attributes:
                        line += f'{4 * " "}{attribute};\n'

                # Replaces the placeholder @Parameters
                if p_parameters.match(line):
                    if parser.name_to_template[name].parameter:
                        parameter_type = parser.name_to_template[name].parameter.type
                        if parameter_type not in ('bool', 'int', 'double', 'string'):
                            p_alias = re.compile(r'^\s*(typedef)\s*(\w*)\s*(\w*)')
                            for alias in parser.global_declaration.aliases:
                                match_alias = p_alias.match(alias)
                                if match_alias:
                                    parameter_type = match_alias.group(2)
                        parameter_identifier = parser.name_to_template[name].parameter.identifier
                        template_parameter = f'{parameter_type} {parameter_identifier};'
                        line = line.replace('@Parameters', template_parameter)
                    else:
                        line = line.replace('@Parameters', '// No template parameters\n')

                # Replaces the placeholder @CloseIncludeGuard
                if p_close_include_guard.match(line):
                    close_include_guard = f'#endif // {name.upper()}_H\n'
                    line = close_include_guard

                file_generated.write(line)
                logging.debug(f"Into {filename} stored: {line}")

            file_generated.close()
            logging.info(f"File {filename} closed")

    @staticmethod
    def _generate_application_implementations(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates one implementation file for each uppaal template.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename_template = 'Application.cc'

        # Generates one implementation file for each uppaal template
        for name in parser.name_to_template.keys():
            filename = f'{upper_first_letter(name)}.cc'
            path_dst_file = os.path.join(Interpreter._get_application_folder_name(path_dst, name), filename)
            file_generated = open(path_dst_file, 'w')
            logging.info(f'File {filename} opened')

            # Gets the file template
            pathlist = [os.path.dirname(__file__),
                        Interpreter.rel_path_template,
                        Interpreter.rel_path_application_src,
                        filename_template]
            path_file_template = os.path.join(*pathlist)
            with open(path_file_template, 'r') as file_template:
                content_template = file_template.read()

            # Pattern for matching the placeholder '@IncludeHeader'
            p_include_header = re.compile(r'^(.*)@IncludeHeader')

            # Pattern for matching the placeholder '@DefineModule'
            p_define_module = re.compile(r'^(.*)@DefineModule')

            # Pattern for matching the placeholder '@ClassName'
            p_class_name = re.compile(r'^(.*)@ClassName')

            # Pattern for matching the placeholder '@InitializeUppalTemplate'
            p_initialize_uppaal_template = re.compile(r'^(.*)@InitializeUppalTemplate')

            # Pattern for matching the placeholder '@Methods'
            p_methods = re.compile(r'^@Methods')

            # Writes the file
            lines = content_template.splitlines(keepends=True)
            for line in lines:
                # Replaces the placeholder @IncludeHeader
                if p_include_header.match(line):
                    line = f'#include "{upper_first_letter(name)}.h"\n'

                # Replaces the placeholder @DefineModule
                if p_define_module.match(line):
                    line = line.replace('@DefineModule', upper_first_letter(name))

                # Replaces the placeholder @ClassName
                if p_class_name.match(line):
                    line = line.replace('@ClassName', upper_first_letter(name))

                # Replaces the placeholder @InitializeUppalTemplate
                if p_initialize_uppaal_template.match(line):
                    template = parser.name_to_template[name]
                    line = f'{4 * " "}uppaalTemplate.setName("{template.name}");\n'
                    for location in template.locations:
                        line += f'{4 * " "}uppaalTemplate.addStateID("{location}");\n'
                    line += f'{4 * " "}uppaalTemplate.setCurrentStateID("{template.init}");\n'
                    transition_suffix = upper_first_letter(name)
                    i = 0
                    for transition in template.transitions:
                        transition_name = get_transition_name(i, suffix=transition_suffix)

                        line += (f'{4 * " "}uppaalTemplate.addTransition(new {transition_name}('
                                 f'"{transition_name}", '
                                 f'"{transition.source}", '
                                 f'"{transition.target}", '
                                 f'check_and_cast<VirtualApplication*>(this)));\n'
                                 )
                        i += 1

                # Replaces the placeholder @Methods
                if p_methods.match(line):
                    line = ''

                    # Pattern for matching the functions' signatures
                    #
                    # Capturing groups:
                    #  group(1) : the type of the method
                    #  group(2) : the remaining signature of the method
                    p_signature = re.compile(r'^\s*(\w*)\s*(.+)')

                    # Pattern for matching the placeholder '// @Trace'
                    # Capturing groups
                    #  group(1) : the text of the trace
                    #  group(2) : the variable to be traced
                    p_trace = re.compile(r'^\s*//\s*@Trace\((.*),(.*)\)\s*')

                    functions = parser.name_to_template[name].declaration.functions
                    signatures = parser.name_to_template[name].declaration.signatures
                    i = 0

                    for function in functions:
                        # Mangles the signature in the function blotter
                        blotter_function = ''
                        match_signature = p_signature.match(signatures[i])
                        blotter_function += (f'{match_signature.group(1).strip()} '
                                             f'{upper_first_letter(name)}::{match_signature.group(2).strip()}'
                                             )
                        blotter_function += function.replace(signatures[i], '')
                        blotter_function += '\n'

                        # Slits the blotter over lines and resolve @Trace placeholders
                        blotter_lines = blotter_function.splitlines(keepends=True)
                        for blotter_line in blotter_lines:
                            match_trace = p_trace.match(blotter_line)
                            if match_trace:
                                blotter_line = (f'{4 * " "}trace() << '
                                                f'{match_trace.group(1).strip()} << {match_trace.group(2).strip()};\n'
                                                )

                            line += blotter_line

                        i += 1

                file_generated.write(line)
                logging.debug(f"Into {filename} stored: {line}")

            file_generated.close()
            logging.info(f"File {filename} closed")

    @staticmethod
    def _generate_transition_headers(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates the headers of all the transitions.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename_template = 'Transition.h'

        # Pattern for matching the placeholder '@OpenIncludeGuard'
        p_open_include_guard = re.compile(r'^(.*)@OpenIncludeGuard')

        # Pattern for matching the placeholder '@ClassName'
        p_class_name = re.compile(r'^(.*)@ClassName')

        # Pattern for matching the placeholder '@CloseIncludeGuard'
        p_close_include_guard = re.compile(r'^(.*)@CloseIncludeGuard')

        for name in parser.name_to_template.keys():
            i = 0
            for _ in parser.name_to_template[name].transitions:
                template_suffix = upper_first_letter(name)
                transition_classname = get_transition_name(i, template_suffix)
                filename = f'{transition_classname}.h'

                path_file_dst = os.path.join(Interpreter._get_application_folder_name(path_dst, name), filename)
                file_generated = open(path_file_dst, 'w')
                logging.info(f'File {filename} opened')

                # Gets the file template
                pathlist = [os.path.dirname(__file__),
                            Interpreter.rel_path_template,
                            Interpreter.rel_path_application_src,
                            filename_template]
                path_file_template = os.path.join(*pathlist)
                with open(path_file_template, 'r') as file_template:
                    content_template = file_template.read()

                    # Writes the file
                    lines = content_template.splitlines(keepends=True)
                    for line in lines:
                        # Replaces the placeholder @OpenIncludeGuard
                        if p_open_include_guard.match(line):
                            open_include_guard = f'#ifndef {transition_classname.upper()}_H\n'
                            open_include_guard += f'#define {transition_classname.upper()}_H\n'
                            line = open_include_guard

                        # Replaces the placeholder @ClassName
                        if p_class_name.match(line):
                            line = line.replace('@ClassName', transition_classname)

                        # Replaces the placeholder @CloseIncludeGuard
                        if p_close_include_guard.match(line):
                            close_include_guard = f'#endif // {transition_classname.upper()}_H\n'
                            line = close_include_guard

                        file_generated.write(line)
                        logging.debug(f'Into {filename} stored: {line}')

                file_generated.close()
                logging.info(f"File {filename} closed")

                i += 1

    @staticmethod
    def _generate_transition_implementations(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates the implementations of all the transitions.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename_template = 'Transition.cc'

        # Pattern for matching the placeholder '@IncludeClassHeader
        p_include_class_header = re.compile(r'^\s*@IncludeClassHeader\s*')

        # Pattern for matching the placeholder '@IncludeUppaalPacket
        p_include_uppaal_packet = re.compile(r'^\s*@IncludeUppaalPacket\s*')

        # Pattern for matching the placeholder '@ClassName
        p_classname = re.compile(r'^(.*)@ClassName(.*)')

        # Pattern for matching the placeholder '@Guard
        p_guard = re.compile(r'^\s*@Guard\s*')

        # Pattern for matching the placeholder '@Synchronization'
        p_synchronization = re.compile(r'^\s*@Synchronization\s*')

        # Pattern for matching the placeholder '@Assignments'
        p_assignments = re.compile(r'^\s*@Assignments\s*')

        # Pattern for matching the synchronization channel
        #
        # Capturing groups:
        #  group(1) : the identifier of the channel
        #  group(2) : the channel
        p_synchronization_channel = re.compile(r'^\s*(\w+)\s*\[(\w+)\][!|?]\s*')

        for name in parser.name_to_template.keys():
            i = 0
            for transition in parser.name_to_template[name].transitions:
                template_suffix = upper_first_letter(name)
                transition_classname = get_transition_name(i, template_suffix)
                filename = f'{transition_classname}.cc'
                path_file_dst = os.path.join(Interpreter._get_application_folder_name(path_dst, name), filename)
                file_generated = open(path_file_dst, 'w')
                logging.info(f"File {filename} opened")

                # Gets the file template
                pathlist = [os.path.dirname(__file__),
                            Interpreter.rel_path_template,
                            Interpreter.rel_path_application_src,
                            filename_template]
                path_file_template = os.path.join(*pathlist)
                with open(path_file_template, 'r') as file_template:
                    content_template = file_template.read()

                    # Caches the lines
                    include_uppaal_packet = False
                    lines_to_write: List[str] = []
                    lines = content_template.splitlines(keepends=True)
                    for line in lines:
                        # Replaces the placeholder @IncludeClassHeader
                        if p_include_class_header.match(line):
                            line = f'#include "{transition_classname}.h"\n'
                            line += f'#include "{template_suffix}.h"\n'

                        # Replaces the placeholder @ClassName
                        if p_classname.match(line):
                            line = line.replace('@ClassName', transition_classname)

                        # Replaces the placeholder @Guard
                        if p_guard.match(line):
                            if transition.guard:
                                # Checks if the identifiers are local
                                identifiers = re.findall(r'[\w]+', transition.guard)
                                identifier_to_mangled: Dict[str, str] = {}
                                for identifier in identifiers:
                                    identifier = identifier.strip()
                                    # Skips digits
                                    if identifier.isdigit():
                                        continue

                                    # Checks if the identifier is global
                                    if not is_global_identifier(identifier, parser.global_declaration.globals):
                                        identifier_to_mangled[identifier] = (f'check_and_cast<'
                                                                             f'{template_suffix}'
                                                                             f'*>(application_)->'
                                                                             f'{identifier}'
                                                                             )

                                guard_mangled = transition.guard
                                for identifier in identifier_to_mangled.keys():
                                    guard_mangled = guard_mangled.replace(identifier, identifier_to_mangled[identifier])

                                line = f'{4 * " "} return {guard_mangled};\n'
                            else:
                                line = f'{4 * " "}// No guard\n'
                                line += f'{4 * " "}return true;\n'

                        # Replaces the placeholder @Synchronization
                        if p_synchronization.match(line):
                            if transition.synchronization:
                                include_uppaal_packet = True

                                # Gets the channel
                                match_channel = p_synchronization_channel.match(transition.synchronization)
                                channel: str = ''
                                if match_channel:
                                    channel += match_channel.group(2)
                                else:
                                    error_msg = f"Cannot get the channel in: {transition.synchronization}"
                                    logger.error(error_msg)
                                    raise RuntimeError(error_msg)

                                # Checks if the variable is a digit or a variable
                                if not channel.isdigit():
                                    if not is_global_identifier(channel, parser.global_declaration.globals):
                                        channel = f'check_and_cast<{template_suffix}*>(application_)->{channel}'

                                # Rx synchronization
                                if '?' in transition.synchronization:
                                    line = (f'{4 * " "}bool found = false;\n'
                                            f'{4 * " "}int index = 0;\n'
                                            f'{4 * " "}for ( auto const& data : check_and_cast<{template_suffix}'
                                            f'*>(application_)->rxBuffer )\n'
                                            f'{4 * " "}{{\n'
                                            f'{8 * " "}if ( data.senderNodeID == " + {channel} + " )\n'
                                            f'{8 * " "}{{\n'
                                            f'{12 * " "}found = true;\n'
                                            f'{12 * " "}break;\n'
                                            f'{8 * " "}}}\n'
                                            f'{8 * " "}index++;\n'
                                            f'{4 * " "}}}\n'
                                            f'{4 * " "}if ( found )\n'
                                            f'{4 * " "}{{\n'
                                            f'{8 * " "}check_and_cast<{template_suffix}*>'
                                            f'(application_)->rxBuffer.erase(check_and_cast<{template_suffix}'
                                            f'*>(application_)->rxBuffer.begin() + index);\n'
                                            f'{4 * " "}}}\n'
                                            f'{4 * " "}return found;\n'
                                            )

                                # Tx synchronization
                                elif '!' in transition.synchronization:
                                    line = (f'{4 * " "}UppaalData uppaalData;\n'
                                            f'{4 * " "}uppaalData.senderNodeID = "{channel}";\n'
                                            f'{4 * " "}UppaalPacket *packetToNet;\n'
                                            f'{4 * " "}packetToNet = new UppaalPacket('
                                            f'"UppaalPacket", APPLICATION_PACKET);\n'
                                            f'{4 * " "}packetToNet->setUppaalData(uppaalData);\n'
                                            f'{4 * " "}check_and_cast<{template_suffix}*>(application_)->'
                                            f'toNetworkLayer(packetToNet, BROADCAST_NETWORK_ADDRESS);\n'
                                            )

                                # Cannot handle synchronization
                                else:
                                    error_msg = f"Cannot handle synchronization: {transition.synchronization}"
                                    logger.error(error_msg)
                                    raise RuntimeError(error_msg)
                            else:
                                line = (f'{4 * " "}// No synchronization\n'
                                        f'{4 * " "}return true;\n')

                        # Replaces the placeholder @Assignments
                        if p_assignments.match(line):
                            line = ''
                            if transition.assignments:
                                # Splits on comma and rebuild function calls
                                splitted_assignments = transition.assignments.split(',')
                                assignments: List[str] = []
                                merge = ''
                                for splitted_assignment in splitted_assignments:
                                    if '(' in splitted_assignment and ')' not in splitted_assignment:
                                        merge = splitted_assignment
                                        continue
                                    if merge and ')' in splitted_assignment:
                                        merge += ', ' + splitted_assignment
                                        assignments.append(merge)
                                        merge = ''
                                        continue
                                    assignments.append(splitted_assignment)

                                for assignment in assignments:
                                    assignment = assignment.strip()
                                    assignment = assignment.replace(':=', '=')

                                    identifiers_in_assignment = re.findall(r'[\w]+', assignment)
                                    identifier_to_mangled: Dict[str, str] = {}
                                    skip_clock = False
                                    for identifier in identifiers_in_assignment:
                                        identifier = identifier.strip()
                                        # Skips the clock
                                        if identifier == parser.global_declaration.clock:
                                            skip_clock = True
                                            break

                                        # Skips digits
                                        if identifier.isdigit():
                                            continue

                                        # Checks if the identifier is global
                                        if not is_global_identifier(identifier, parser.global_declaration.globals):
                                            identifier_to_mangled[identifier] = (f'check_and_cast<{template_suffix}'
                                                                                 f'*>(application_)->{identifier}')

                                    if skip_clock:
                                        continue

                                    for identifier in identifier_to_mangled.keys():
                                        assignment = assignment.replace(identifier, identifier_to_mangled[identifier])

                                    line += f'({4 * " "}{assignment};\n'

                            else:
                                line = f'{4 * " "}// No assignments\n'

                        lines_to_write.append(line)

                    # Replaces the placeholder @IncludeUppaalPacket
                    for line in lines_to_write:
                        if p_include_uppaal_packet.match(line):
                            if include_uppaal_packet:
                                line = f'#include "UppaalPacket_m.h"\n'
                            else:
                                line = ''

                        file_generated.write(line)
                        logging.debug(f"Into {filename} stored: {line}")

                file_generated.close()
                logging.info(f"File {filename} closed")

                i += 1

    @staticmethod
    def _generate_state_machine_headers(path_dst: str) -> None:
        """
        Generates the headers of the state machine.

        :param path_dst: the destination directory
        """
        # State machine's core
        filename = 'UppaalTemplate.h'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # UppaalTemplate.h has not to be modified
        file_generated.write(content_template)
        file_generated.close()

        # State machine's abstract transition
        filename = 'AbstractTransition.h'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # UppaalTemplate.h has not to be modified
        file_generated.write(content_template)
        file_generated.close()

    @staticmethod
    def _generate_state_machine_implementations(path_dst: str) -> None:
        """
        Generates the implementation files of the state machine.

        :param path_dst: the destination directory
        """
        # State machine's core
        filename = 'UppaalTemplate.cc'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # UppaalTemplate.h has not to be modified
        file_generated.write(content_template)
        file_generated.close()

        # State machine's abstract transition
        filename = 'AbstractTransition.cc'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_src,
                    Interpreter.folder_util_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # UppaalTemplate.cc has not to be modified
        file_generated.write(content_template)
        file_generated.close()

    @staticmethod
    def _generate_omnetpp_ini(path_dst: str, parser: XmlUppaalParser) -> None:
        """
        Generates the file omnetpp.ini.

        :param path_dst: the destination directory
        :param parser: the parser for the uppaal model
        """
        filename = 'omnetpp.ini'

        file_generated = open(os.path.join(path_dst, filename), 'w')
        logging.info(f"File {filename} opened")

        # Gets the file template
        pathlist = [os.path.dirname(__file__),
                    Interpreter.rel_path_template,
                    Interpreter.rel_path_application_sim,
                    Interpreter.folder_sim_application,
                    filename]
        path_file_template = os.path.join(*pathlist)
        with open(path_file_template, 'r') as file_template:
            content_template = file_template.read()

        # Writes the heading
        heading = get_heading(parser.xml_source, '#')
        file_generated.write(heading)

        # Pattern for matching the placeholder '@SimulationField'
        p_simulation_field = re.compile(r'^\s*@SimulationField\s*$')

        # Pattern for matching the placeholder '@NumberOfNodes'
        p_number_of_nodes = re.compile(r'^\s*@NumberOfNodes\s*$')

        # Pattern for matching the placeholder '@NodesPosition'
        p_nodes_position = re.compile(r'^\s*@NodesPosition\s*$')

        # Pattern for matching the placeholder '@ApplicationName'
        p_application_name = re.compile(r'^\s*@ApplicationName\s*$')

        # Pattern for matching the placeholder '@ApplicationParam'
        p_application_param = re.compile(r'^\s*@ApplicationParam\s*$')

        # Writes the file
        lines = content_template.splitlines(keepends=True)
        for line in lines:
            # Replaces the placeholders
            if p_simulation_field.match(line):
                [field_x, field_y, field_z] = Interpreter._calculate_simulation_field(parser.system)

                line_field_x = f'SN.field_x = {field_x}\n'
                logging.debug(f"Into {filename} stored: {line_field_x}")
                file_generated.write(line_field_x)

                line_field_y = f'SN.field_y = {field_y}\n'
                logging.debug(f"Into {filename} stored: {line_field_y}")
                file_generated.write(line_field_y)

                line_field_z = f'SN.field_z = {field_z}\n'
                logging.debug(f"Into {filename} stored: {line_field_z}")
                file_generated.write(line_field_z)

            elif p_number_of_nodes.match(line):
                line_number_of_nodes = f'SN.numNodes = {len(parser.system.nodes)}\n'
                logging.debug(f"Into {filename} stored: {line_number_of_nodes}")
                file_generated.write(line_number_of_nodes)

            elif p_nodes_position.match(line):
                i = 0
                for node in parser.system.nodes:
                    line_node_x = f'SN.node[{i}].xCoor = {node.coord.x}\n'
                    logging.debug(f"Into {filename} stored: {line_node_x}")
                    file_generated.write(line_node_x)

                    line_node_y = f'SN.node[{i}].yCoor = {node.coord.y}\n'
                    logging.debug(f"Into {filename} stored: {line_node_y}")
                    file_generated.write(line_node_y)

                    line_node_z = f'SN.node[{i}].zCoor = {node.coord.z}\n'
                    logging.debug(f"Into {filename} stored: {line_node_z}")
                    file_generated.write(line_node_z)

                    i = i + 1

            elif p_application_name.match(line):
                i = 0
                for node in parser.system.nodes:
                    line_application_name = (f'SN.node[{i}].ApplicationName = '
                                             f'"{upper_first_letter(node.template_name)}"\n'
                                             )
                    logging.debug(f"Into {filename} stored: {line_application_name}")
                    file_generated.write(line_application_name)
                    i = i + 1

            elif p_application_param.match(line):
                i = 0
                for node in parser.system.nodes:
                    template = parser.name_to_template[node.template_name]
                    if template.parameter:
                        line_parameter_name = f'SN.node[{i}].Application.{template.parameter.identifier} = '
                        if template.parameter.type == 'string':
                            line_parameter_name += f'\"{node.argument}\"'
                        else:
                            line_parameter_name += node.argument
                        line_parameter_name += '\n'
                        logging.debug(f"Into {filename} stored: {line_parameter_name}")
                        file_generated.write(line_parameter_name)
                    i = i + 1

            else:
                file_generated.write(line)
                logging.debug(f"Into {filename} stored: {line}")

        file_generated.close()
        logging.info(f"File {filename} closed")

    @staticmethod
    def interpret(path_dst: str, xml_file_path: str, layer: Layer = Layer.APPLICATION) -> None:
        """
        Interprets the given xml file and produces the following files:
         - global.h
         - omnetpp.ini
         - .ned modules
         - .h/.cpp application modules
        Files are stored into the given destination dir.

        :param path_dst: dhe destination path
        :param xml_file_path: the path to the xml file to interpret.
        :param layer: the target layer, application by default
        """

        # Parses the source file
        parser = XmlUppaalParser()
        parser.parse(xml_file_path)

        if layer == Layer.APPLICATION:
            logger.info(f"Interpreting {xml_file_path} as {layer.name}")

            # Builds the directories for storing files
            logger.info("Building application layer dirs")
            Interpreter._make_application_directories(path_dst, parser)

            # Generates the global dependencies
            logger.info("Generating source global dependencies")
            path_application_util = Interpreter._get_path_application_util(path_dst)
            Interpreter._generate_global_dependencies(path_application_util, parser)

            # Generates the msg file
            Interpreter._generate_packet_msg(path_application_util, parser)

            # Generate the header of the state machine
            Interpreter._generate_state_machine_headers(path_application_util)

            # Generate the implementation of the state machine
            Interpreter._generate_state_machine_implementations(path_application_util)

            # Generates nodes' applications
            logger.info("Generating source applications")
            path_application_base = os.path.join(path_dst, Interpreter.rel_path_application_src)

            # Generates the ned files
            Interpreter._generate_ned_module(path_application_base, parser)

            # Generates the application header files
            Interpreter._generate_application_headers(path_application_base, parser)

            # Generates the application implementation files
            Interpreter._generate_application_implementations(path_application_base, parser)

            # Generate the headers of the transitions
            Interpreter._generate_transition_headers(path_application_base, parser)

            # Generate the implementations of the transitions
            Interpreter._generate_transition_implementations(path_application_base, parser)

            # Generates the file omnetpp.ini
            logger.info("Generating simulation")
            path_application_sim = os.path.join(
                os.path.join(path_dst, Interpreter.rel_path_application_sim),
                Interpreter.folder_sim_application)
            Interpreter._generate_omnetpp_ini(path_application_sim, parser)

        else:
            error_msg = f"Layer {layer.name} not supported"
            logger.error(error_msg)
            raise ValueError(error_msg)
