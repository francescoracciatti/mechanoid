#include "AbstractTransition.h"

#include <utility>

std::string AbstractTransition::getSourceStateID() const
{
    return sourceStateID_;
}

std::string AbstractTransition::getTargetStateID() const
{
    return targetStateID_;
}

AbstractTransition::~AbstractTransition()
{
}

AbstractTransition::AbstractTransition(std::string name,
                       std::string sourceStateID,
                       std::string targetStateID,
                       VirtualApplication *application)
        : name_(std::move(name))
        , sourceStateID_(std::move(sourceStateID))
        , targetStateID_(std::move(targetStateID))
        , application_(application)
{
}
