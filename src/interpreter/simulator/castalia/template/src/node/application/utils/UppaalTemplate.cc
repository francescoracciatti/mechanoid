#include "UppaalTemplate.h"

#include <utility>

UppaalTemplate::UppaalTemplate()
{
}

void UppaalTemplate::setName(const std::string &name)
{
    name_ = name;
}

void UppaalTemplate::addStateID(const std::string &stateID)
{
    stateIDs_.push_back(stateID);
    stateIDToTransitions_.insert(
            std::pair<std::string, std::vector<AbstractTransition*> >(
                    stateID, std::vector<AbstractTransition*>()));
}

void UppaalTemplate::setCurrentStateID(const std::string &stateID)
{
    currentStateID_ = stateID;
}

void UppaalTemplate::addTransition(AbstractTransition *transition)
{
    stateIDToTransitions_.at(transition->getSourceStateID()).push_back(transition);
}

void UppaalTemplate::evolve()
{
    for ( const auto &transition : stateIDToTransitions_.at(currentStateID_) )
    {
        if ( transition->testGuard() )
        {
            if ( transition->doSynchronization() )
            {
                setCurrentStateID(transition->doAssignments());
            }
        }
    }
}
