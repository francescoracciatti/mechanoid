// This file provides the base class for modelling UPPAAL transitions.
//
// Author: Francesco Racciatti
// Copyright: Copyright 2019
// License: MIT

#ifndef ABSTRACT_TRANSITION_H
#define ABSTRACT_TRANSITION_H

#include <string>

#include "VirtualApplication.h"

/**
 * Base class to model UPPAAL transition.
 */
class AbstractTransition {
public:
    /**
     * Tests the guard.
     *
     * @return
     *      true if the guard is satisfied, false otherwise
     */
     virtual bool testGuard() const = 0;

    /**
     * Does the synchronization.
     *
     * @return
     *      true if the node is synchronized, false otherwise
     */
     virtual bool doSynchronization() = 0;

    /**
     * Does the assignments.
     *
     * @return
     *      the identifier of the target state
     */
    virtual std::string doAssignments() = 0;

    /**
     * Gets the identifier of the source state.
     *
     * @return
     *      the identifier of the source state
     */
    virtual std::string getSourceStateID() const;

    /**
     * Gets the identifier of the target state.
     *
     * @return
     *      the identifier of the target state
     */
    virtual std::string getTargetStateID() const;

    /**
     * Dtor.
     */
    virtual ~AbstractTransition();

protected:
    /**
     * Ctor.
     *
     * @param[in] name
     *              The name of the transition
     *
     * @param[in] sourceStateID
     *              The source state
     *
     * @param[in] targetStateID
     *              The target state
     *
     * @param[in] application
     *              The application to which the transition refers to
     */
    AbstractTransition(std::string name, std::string sourceStateID,
                        std::string targetStateID, VirtualApplication *application);

protected:
    // The name of the transition
    std::string name_;

    // The identifier of the source state
    std::string sourceStateID_;

    // The identifier of the target state
    std::string targetStateID_;

    // The application to which the transition refers to
    VirtualApplication *application_;
};

#endif //ABSTRACT_TRANSITION_H
