// This file provides the model for UPPAAL transitions.
//
// Author: Francesco Racciatti
// Copyright: Copyright 2019
// License: MIT

@OpenIncludeGuard

#include "AbstractTransition.h"

/**
 * This class models an UPPAAL transition.
 */
class @ClassName : public AbstractTransition {
public:
    /**
     * Ctor.
     *
     * @param[in] name
     *              The name of the transition
     *
     * @param[in] sourceStateID
     *              The source state
     *
     * @param[in] targetStateID
     *              The target state
     *
     * @param[in] application
     *              The application to which the transaction refers to
     */
    @ClassName(std::string name, std::string sourceStateID, std::string targetStateID, VirtualApplication *application);

    /**
     * Tests the guard.
     *
     * @return
     *      true if the guard is satisfied, false otherwise
     */
     bool testGuard() const;

    /**
     * Does the synchronization.
     *
     * @return
     *      true if the node is synchronized, false otherwise
     */
     bool doSynchronization();

    /**
     * Does the assignments.
     *
     * @return
     *      the identifier of the target state
     */
    std::string doAssignments();
};

@CloseIncludeGuard
