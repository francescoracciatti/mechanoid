// This file provides the model for UPPAAL templates.
//
// Author: Francesco Racciatti
// Copyright: Copyright 2019
// License: MIT

#ifndef UPPAAL_TEMPLATE_H
#define UPPAAL_TEMPLATE_H

#include <string>
#include <vector>
#include <map>

#include "AbstractTransition.h"
#include "UppaalPacket_m.h"

/**
 * This class models an UPPAAL template.
 */
class UppaalTemplate {
public:
    /**
     * Ctor.
     */
    UppaalTemplate();

    /**
     * Sets the name of the template.
     *
     * @param[in] name
     *              the name to set
     */
    void setName(const std::string &name);

    /**
     * Adds the identifier of the state.
     *
     * @param[in] state
     *              the state to add
     */
    void addStateID(const std::string &stateID);

    /**
     * Sets the identifier of the current state.
     *
     * @param[in] stateID
     *                  the id of the state to set
     */
    void setCurrentStateID(const std::string &stateID);

    /**
     * Adds the given transition.
     *
     * @param[in] transition
     *                  the transition to add
     */
    void addTransition(AbstractTransition *transition);

    /**
     * Goes to the next state, if possible.
     */
    void evolve();

private:
    // The name of the template
    std::string name_;

    // The identifier of the current state
    std::string currentStateID_;

    // The states
    std::vector<std::string> stateIDs_;

    // The map of transitions, maps each starting status with the set of its transitions
    std::map<std::string, std::vector<AbstractTransition*> > stateIDToTransitions_;
};

#endif //UPPAAL_TEMPLATE_H
