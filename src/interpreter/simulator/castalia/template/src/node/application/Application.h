@OpenIncludeGuard

#include <vector>

#include "UppaalPacket_m.h"
#include "UppaalTemplate.h"
#include "uppaal_globals.h"

@IncludeTransitions

enum ProcessingTimers {
    CHANGE_STATE = 1,
};

class @ClassName : public VirtualApplication {
friend class AbstractTransition;
@FriendList

protected:
    void startup();
    void timerFiredCallback(int);
    void fromNetworkLayer(ApplicationPacket *, const char *, double, double);
    void finishSpecific();

private:
    void traceMessage(std::string msg);

    @Methods

private:
    // The rx buffer
    std::vector<UppaalData> rxBuffer;

    // The processing period in milliseconds
    int processingPeriodMillis;

    // The UPPAAL template
    UppaalTemplate uppaalTemplate;

    // The UPPAAL template's attributes
    @Attributes

    // The UPPAAL template's parameter
    @Parameters
};

@CloseIncludeGuard
