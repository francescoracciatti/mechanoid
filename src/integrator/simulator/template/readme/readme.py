"""
This file provide the class for building the README.txt file.
"""

from typing import Dict


class Readme(object):
    """
    This class provides information for building the file README.txt.

    The template file README.txt contains the following placeholders to be replaced:
     - @Project, with the project name;
     - @Version, with the version of the project;
     - @Timestamp, with the current timestamp;
     - @FolderCustom, with the name of the folder containing target customized files;
     - @FolderNetwork, with the name of the folder containing the network model;
     - @FolderSource, with the name of the folder containing the source model;
     - @FolderTarget, with the name of the folder containing the target platform;
     - @Target, with the target platform name.
    """

    # Placeholders
    PLACEHOLDER_PROJECT = '@Project'
    PLACEHOLDER_VERSION = '@Version'
    PLACEHOLDER_TIMESTAMP = '@Timestamp'
    PLACEHOLDER_FOLDER_CUSTOM = '@FolderCustom'
    PLACEHOLDER_FOLDER_NETWORK = '@FolderNetwork'
    PLACEHOLDER_FOLDER_SOURCE = '@FolderSource'
    PLACEHOLDER_FOLDER_TARGET = '@FolderTarget'
    PLACEHOLDER_TARGET = '@Target'

    # List of placeholders
    _PLACEHOLDERS = [PLACEHOLDER_PROJECT,
                     PLACEHOLDER_VERSION,
                     PLACEHOLDER_TIMESTAMP,
                     PLACEHOLDER_FOLDER_CUSTOM,
                     PLACEHOLDER_FOLDER_NETWORK,
                     PLACEHOLDER_FOLDER_SOURCE,
                     PLACEHOLDER_FOLDER_TARGET,
                     PLACEHOLDER_TARGET]

    def __init__(self) -> None:
        """
        Ctor.
        """
        # Dictionary containing placeholders and related values
        self._readme: Dict[str, str] = {}
        # Loads the placeholders
        for placeholder in self._PLACEHOLDERS:
            self._readme[placeholder] = ''

    def __repr__(self) -> str:
        """
        Repr.

        :return: the string representing the instance of the class.
        """
        return str(self.__dict__)

    def add_project(self, project: str) -> None:
        """
        Adds the project related to the placeholder @Project.

        :param project: the project
        """
        self._readme[self.PLACEHOLDER_PROJECT] = project

    def get_project(self) -> str:
        """
        Gets the value related to the placeholder @Project.

        :return: the value related to the placeholder @Project.
        """
        return self._readme[self.PLACEHOLDER_PROJECT]

    def add_version(self, version: str) -> None:
        """
        Adds the version related to the placeholder @Version.

        :param version: the version
        """
        self._readme[self.PLACEHOLDER_VERSION] = version

    def get_version(self) -> str:
        """
        Gets the value related to the placeholder @Version.

        :return: the value related to the placeholder @Version.
        """
        return self._readme[self.PLACEHOLDER_VERSION]

    def add_timestamp(self, timestamp: str) -> None:
        """
        Adds the timestamp related to the placeholder @Timestamp.

        :param timestamp: the timestamp
        """
        self._readme[self.PLACEHOLDER_TIMESTAMP] = timestamp

    def get_timestamp(self) -> str:
        """
        Gets the value related to the placeholder @Timestamp.

        :return: the value related to the placeholder @Timestamp.
        """
        return self._readme[self.PLACEHOLDER_TIMESTAMP]

    def add_folder_custom(self, target: str) -> None:
        """
        Adds the target related to the placeholder @FolderCustom.

        :param target: the target
        """
        self._readme[self.PLACEHOLDER_FOLDER_CUSTOM] = target

    def get_folder_custom(self) -> str:
        """
        Gets the value related to the placeholder @FolderCustom.

        :return: the value related to the placeholder @FolderCustom
        """
        return self._readme[self.PLACEHOLDER_FOLDER_CUSTOM]

    def add_folder_network(self, target: str) -> None:
        """
        Adds the target related to the placeholder @FolderNetwork.

        :param target: the target
        """
        self._readme[self.PLACEHOLDER_FOLDER_NETWORK] = target

    def get_folder_network(self) -> str:
        """
        Gets the value related to the placeholder @FolderNetwork.

        :return: the value related to the placeholder @FolderNetwork
        """
        return self._readme[self.PLACEHOLDER_FOLDER_NETWORK]

    def add_folder_source(self, target: str) -> None:
        """
        Adds the target related to the placeholder @FolderSource.

        :param target: the target
        """
        self._readme[self.PLACEHOLDER_FOLDER_SOURCE] = target

    def get_folder_source(self) -> str:
        """
        Gets the value related to the placeholder @FolderSource.

        :return: the value related to the placeholder @FolderSource
        """
        return self._readme[self.PLACEHOLDER_FOLDER_SOURCE]

    def add_folder_target(self, target: str) -> None:
        """
        Adds the target related to the placeholder @FolderTarget.

        :param target: the target
        """
        self._readme[self.PLACEHOLDER_FOLDER_TARGET] = target

    def get_folder_target(self) -> str:
        """
        Gets the value related to the placeholder @FolderTarget.

        :return: the value related to the placeholder @FolderTarget
        """
        return self._readme[self.PLACEHOLDER_FOLDER_TARGET]

    def add_target(self, target: str) -> None:
        """
        Adds the target related to the placeholder @Target.

        :param target: the target
        """
        self._readme[self.PLACEHOLDER_TARGET] = target

    def get_target(self) -> str:
        """
        Gets the value related to the placeholder @Target.

        :return: the value related to the placeholder @Target
        """
        return self._readme[self.PLACEHOLDER_TARGET]

    def replace_placeholders(self, template: str) -> str:
        """
        Replaces the placeholders from the input string.

        :param template: the input template string
        :return: the string after replacing placeholders
        """
        for placeholder in self._readme.keys():
            template = template.replace(placeholder, self._readme[placeholder])
        return template
