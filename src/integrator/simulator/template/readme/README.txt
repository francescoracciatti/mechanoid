This file was autogenerated by @Project @Version on @Timestamp.

The folder '@Project' contains data and files that were automatically generated by @Project. In detail:
 - the folder '@FolderCustom' contains customized files of the target platform;
 - the folder '@FolderNetwork' contains the network model generated by @Project;
 - the folder '@FolderSource' contains the source model.

The folder '@FolderTarget' contains the ready-to-use @Target platform, which already bundles
both the target customizations and the network model generated from the source model.
