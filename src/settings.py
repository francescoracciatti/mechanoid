"""
This file contains global info.
"""

__author__ = 'Francesco Racciatti'
__copyright__ = 'Copyright 2019'
__license__ = 'MIT'
__project__ = 'mechanoid'
__version__ = '1.0.0'
