"""
Unit test for the class Mechanoid.
"""

from unittest import TestCase
from datetime import datetime
import logging

from src.mechanoid import Mechanoid

# Sets the logger
now = datetime.now()
logger = logging.getLogger(__name__)


class TestMechanoid(TestCase):
    """
    This class provides unit tests for the class Mechanoid.
    """

    def test_generate_original_network_model(self):
        """
        Generates the original Castalia simulator.
        :return:
        """
        logger.info("Test: Generate the original network model")

        relative_path_output_castalia = "../../model/output/castalia/"
        target = 'castalia'
        Mechanoid.generate_network_model(relative_path_output_castalia, target)

        logger.info("Test ended")
