# Mechanoid

Mechanoid is a framework that bundles the abstract modeler [UPPAAL](http://www.uppaal.org/) 
with the WSN simulator [Castalia](https://github.com/boulis/Castalia).   

By using Mechanoid you can automatically get the network model of a WSN system starting from its abstract model.
The resulting network model is fully tunable and customizable, and can be used to assess the concrete behavior of the 
abstract model.

As an example, you can build the UPPAAL model of an application (which may also include cyber-attacks),
<p><img src="./img/uppaal_model.png" width="700" height="261"/></p>

define the netwotk topology,
<p><img src="./img/network_topology.png" width="300" height="230" /></p>

and then, you can immediately turn it in a network model and run simulations for assessing the behavior of the system, 
like the nodes' energy consumption: 
<p><img src="./img/energy_consumption.png" width="500" height="363" /></p>

\
So, by using Mechanoid you can:
 * automatically obtain a Castalia network model from a UPPAAL abstract model, no coding required;  
 * assess the behavior of the WSN from the first stages of design;
 * refine the initial abstract model, by exploiting the data provided by the network simulation.

Please, note that Mechanoid is a proof of concept.
It currently supports the application layer of the TCP/IP stack only.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and 
testing purposes. 
In the section [Deployment](#deployment) below, you can find notes on how to deploy the resulting 
network model on a live system. 
The section [Built With](#built-with) below, lists the version numbers of the tools used to develop the project and deploy the 
resulting network model. 

### Prerequisites Development Environment

For building the abstract model of a WSN 
and running Mechanoid in order to obtain the related Castalia network model, 
you need: 
 * [UPPAAL 4.0](http://www.uppaal.org/)
 * [Python 3.7.5](https://www.python.org/downloads/release/python-375/);

Please, note that Mechanoid comes bundled 
with [Castalia 3.3](https://gitlab.com/francescoracciatti/castalia/-/tags/3.3.0). 

Mechanoid was developed by using PyCharm CE 19.2 on macOS High Sierra.

### Prerequisite Deployment Environment

For running Castalia, i.e. the network simulator, you need:
 * [Ubuntu 18.04](https://releases.ubuntu.com/18.04/)
 * [OMNeT++ 4.6](https://omnetpp.org/download/old#omnetpp-46_linux). 

Please, note that Castalia 3.3 cannot run on OMNeT 5 or greater.

### Installing

The following instructions refer to your development environment, running PyCharm CE.
 
Before starting, install all the items listed in section [Prerequisites Development Environment](#prerequisite-deployment-environment).
Then, download the project and import it into PyCharm CE. 
In the project's Run/Debug Configuration, set:
 * the Python interpreter;
 * the Script Path to `../mechanoid/src/mechanoid.py` (which contains the `main` function);
 * the Working directory to `../mechanoid/src`.

Consider to use a virtual environment (venv) for enforcing system isolation.

Your development environment is now ready!

### Running the tests

Tests are based on [unittest framework](https://docs.python.org/3/library/unittest.html). 
All tests must pass.

Before running any simulation, make sure the Castalia simulator produced by the tests works 
as expected on the deployment environment (see section [Deployment](#deployment) below). 
After running tests, you will find the Castalia simulator to test in the folder: 
`../mechanoid/model/output/castalia/*-original-castalia`.

### Usage
 
For generating your *target network model*, starting from your UPPAAL abstract model, you have to:
 1. put the xml file describing your UPPAAL abstract model into the folder `../mechanoid/model/input/uppaal`;
 2. run the project under the current configuration.

Then, you will find the resulting Castalia platform, that bundles your target network model, in the folder 
`../mechanoid/model/output/castalia`.  

You got your target Castalia platform!
You can deploy it and run network simulations (see section [Deployment](#deployment) below).

### Samples

For your convenience, the folder `../mechanoid/sample` contains input/output samples:
 - `../mechanoid/sample/input/uppaal` contains a sample UPPAAL model;
 - `../mechanoid/sample/output/castalia` contains the Castalia platform that already bundles the  model.
 
### Deployment

Before running network simulations on your deployment environment,
you need to install all the items listed in section [Prerequisites Deployment Environment](#prerequisites-development-environment).

Then, move the target Castalia platform you've obtained before on your deployment environment.
The home folder may be a good location. 
Please, refer to the official 
[Castalia user manual](https://github.com/boulis/Castalia/blob/master/Castalia%20-%20User%20Manual.docx) 
to build the target Castalia platform and run simulations. 

## Built With

### Development and testing environment
Host environment:
 * macOS High Sierra 10.13.6
 * [PyCharm](https://www.jetbrains.com/pycharm/) 2019.2.5 CE
 * [Python](https://www.python.org/downloads/) 3.7.5
 * [UPPAAL](http://www.uppaal.org/) 4.0.15
 * [VirtualBox](https://www.virtualbox.org/wiki/Downloads) 6.0.14

### Deployment environment
Guest environment:
 * [Ubuntu](https://ubuntu.com/) 18.04
 * [OMNeT++](https://omnetpp.org/) 4.6

## Contributing

Please read our [contributing instructions](CONTRIBUTING.md) and our [code of conduct](CODE_OF_CONDUCT.md),
for details on the process of submitting requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Author

Francesco Racciatti

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) for details.

## Acknowledgments

* Maurizio Palmieri
* Cinzia Bernardeschi
* Gianluca Dini

## Publication
Bernardeschi, C.; Dini, G.; Palmieri, M. and Racciatti, F. (2020). \
[Analysis of Security Attacks in Wireless Sensor Networks: From UPPAAL to Castalia](https://www.scitepress.org/Link.aspx?doi=10.5220/0009380508150824).\
In Proceedings of the 6th International Conference on Information Systems Security and Privacy - Volume 1: ForSE.\
ISBN 978-989-758-399-5, pages 815-824.\
DOI: 10.5220/0009380508150824