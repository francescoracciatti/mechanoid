# Mechanoid Change Log

All notable changes to this project will be documented in this file.

We use [SemVer](https://semver.org/) for versioning.

## [1.0.1] - 2020-10-23
### Changed
* Changed README
* Minor changes in unittest 

## [1.0.0] - 2020-01-02
### Added
* Bundled with Castalia 3.3.
* Added the parser for parsing UPPAAL xml models.
* Added the interpreter for generating Castalia 3.3 compliant network models from UPPAAL xml models.  
* Added the integrator for bundling the generated Castalia compliant network models, with the original Castalia 3.3. 